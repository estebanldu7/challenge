# challenge

Challenge 1: batman recibe un aviso que el acertijo esta planeando algo, batman busca al acertijo en su escondite, pero solo encuentra una pared con el siguente texto:

Challenge 2: batman encuentra un texto en las hojas de la habitacion con el texto: PCAPO

Challenge 3: termina de descifrar el acertijo:
ymnu kt kz qxqaxditrp


Esteban Preciado
2019-03-10


INSTRUCCIONES

- Copie la carpeta en cualquier directorio.
- Ejecute el archivo index.html.
- Se mostrara en pantalla los 3 desafíos de Batman.

Challenge One

 - Presionar el botón “Generar Código”
 -Se mostrara un botón “Mostrar Texto”
 - Podrá visualizar la información 

Challenge Two

 - Presionar el botón “Decodificar Palabra”
 - Se mostrara una ventana.
 - Ingrese el texto a decodificar “PCAPO”
 - Presionar el botón “Decodificar”

Challenge Three

 - Presionar el botón “Decodificar Palabras”
 - Se mostrara una ventana.
 - Ingrese los siguientes textos a decodificar: 

	- Primer texto:  ymnu
	- Segundo texto: kt
	- Tercer texto: kz
	- Cuarto texto: qxqaxditrp

 - Presionar el botón “Decodificar”


La solución que utilice es Javascript con librería JQUERY porque a mi forma de ver es mucho más sencillo y rápido ejecutar este archivo que utilizar un lenguaje como PHP o Python, ya que tendría desplegar en un servidor.

Toda la programación se encuentra desarrollado en un archivo llamado /js/operations.js
Ademas agregue bootstrap para dar estilos a la pagina.
