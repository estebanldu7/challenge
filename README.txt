{\rtf1\ansi\ansicpg1252\cocoartf1671\cocoasubrtf200
{\fonttbl\f0\fswiss\fcharset0 Helvetica;}
{\colortbl;\red255\green255\blue255;}
{\*\expandedcolortbl;;}
\paperw11900\paperh16840\margl1440\margr1440\vieww10800\viewh8400\viewkind0
\pard\tx566\tx1133\tx1700\tx2267\tx2834\tx3401\tx3968\tx4535\tx5102\tx5669\tx6236\tx6803\pardirnatural\partightenfactor0

\f0\fs24 \cf0 Esteban Preciado\
2019-03-10\
\
\
INSTRUCCIONES\
\
- Copie la carpeta en cualquier directorio.\
- Ejecute el archivo index.html.\
- Se mostrara en pantalla los 3 desaf\'edos de Batman.\
\
Challenge One\
\
 - Presionar el bot\'f3n \'93Generar C\'f3digo\'94\
 -Se mostrara un bot\'f3n \'93Mostrar Texto\'94\
 - Podr\'e1 visualizar la informaci\'f3n \
\
Challenge Two\
\
\pard\tx566\tx1133\tx1700\tx2267\tx2834\tx3401\tx3968\tx4535\tx5102\tx5669\tx6236\tx6803\pardirnatural\partightenfactor0
\cf0  - Presionar el bot\'f3n \'93Decodificar Palabra\'94\
 - Se mostrara una ventana.\
\pard\tx566\tx1133\tx1700\tx2267\tx2834\tx3401\tx3968\tx4535\tx5102\tx5669\tx6236\tx6803\pardirnatural\partightenfactor0
\cf0  - Ingrese el texto a decodificar \'93PCAPO\'94\
 - Presionar el bot\'f3n \'93Decodificar\'94\
\
Challenge Three\
\
\pard\tx566\tx1133\tx1700\tx2267\tx2834\tx3401\tx3968\tx4535\tx5102\tx5669\tx6236\tx6803\pardirnatural\partightenfactor0
\cf0  - Presionar el bot\'f3n \'93Decodificar Palabras\'94\
 - Se mostrara una ventana.\
 - Ingrese los siguientes textos a decodificar: \
\
	- Primer texto:  ymnu\
	- Segundo texto: kt\
	- Tercer texto: kz\
	- Cuarto texto: qxqaxditrp\
\
 - Presionar el bot\'f3n \'93Decodificar\'94\
\
\
La soluci\'f3n que utilice es Javascript con librer\'eda JQUERY porque a mi forma de ver es mucho m\'e1s sencillo y r\'e1pido ejecutar este archivo que utilizar un lenguaje como PHP o Python, ya que tendr\'eda desplegar en un servidor.\
\
Toda la programaci\'f3n se encuentra desarrollado en un archivo llamado /js/operations.js\
Ademas agregue bootstrap para dar estilos a la pagina.\
}