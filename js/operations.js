/**
 * Author: Esteban Preciado
 * Date: 2019-03-07
 */

//Main Function
$(function () {

    //Initialize elements
    $btnMatriz = $("#btnMatriz");
    $btnDecodeWord = $("#btnDecodeWord");
    $btnDecodeWords = $("#btnDecodeWords");
    $modalBody = $("#modalBody");
    $modalHeader = $("#modalHeader");

    //Challenge One
    $btnMatriz.click(function () {
        $modalHeader.html("Challenge One");
        $modalBody.html('<input type="button" class="btn btn-warning" onclick="printLetters()" value="Mostrar Texto">' +
                        '<div id="result"></div>')
    });

    //Challenge Two
    $btnDecodeWord.click(function () {
        $modalHeader.html("Challenge Two");
        $modalBody.html('Ingrese palabra a decodificar: <input type="text" id="decode" name="decode"><br>\n' +
                        '<input type="button" class="btn btn-info" onclick="decodeWord()" value="Decodificar">' +
                        '<div id="result"></div>')
        });

    //Challenge Three
    $btnDecodeWords.click(function () {
        $modalHeader.html("Challenge Three");
        $modalBody.html('<div class="form-group">'+
                        'Primera Palabra: <input type="text" id="wordOne" name="decode"><br>\n' +
                        'Segunda Palabra: <input type="text" id="wordTwo" name="decode"><br>\n' +
                        'Tercera Palabra: <input type="text" id="wordThree" name="decode"><br>\n' +
                        'Cuarta Palabra:  <input type="text" id="wordFour" name="decode"><br>\n' +
                        '</div>' +
                        '<input type="button" class="btn btn-info" onclick="decodeWords()" value="Decodificar">' +
                        '<div id="result"></div>')
        });
});

function printLetters() {

    var final = 90; //Letter Z
    var wallText = '';

    for(var start = 66; start <= final; start++){
        for(var i = start; i <= final; i++) {
            wallText = wallText + String.fromCharCode(i);

            if(i === final){
                for(var j = 65; j < start; j++){
                    wallText = wallText + String.fromCharCode(j);
                }

                wallText = wallText + "\n";
            }
        }
    }

    $("#result").html('<div class="alert alert-primary" role="alert">' + wallText  + '</div>');
}

function decodeWord() {
    $decodeTxt = $("#decode").val();
    $arrayDecode =  $decodeTxt.toUpperCase().split('');
    var word = '';

    for(var i=0; i < $arrayDecode.length; i++){
        var calc = $arrayDecode[i].charCodeAt(0) + 12;

        if(calc <= 90){
           word = word + String.fromCharCode(calc);
        }else{
            calc = $arrayDecode[i].charCodeAt(0) - 14;
            word = word + String.fromCharCode(calc);
        }

        $("#result").html('<div class="alert alert-primary" role="alert"> La palabra decodificada es: <strong>' + word + '</strong></div>');
    }
}

function decodeWords() {
    $wordOne = $("#wordOne").val();
    $wordTwo = $("#wordTwo").val();
    $wordThree = $("#wordThree").val();
    $wordFour = $("#wordFour").val();

    $arrayWordOne =  $wordOne.toUpperCase().split('');
    $arrayWordTwo =  $wordTwo.toUpperCase().split('');
    $arrayWordThree =  $wordThree.toUpperCase().split('');
    $arrayWordFour =  $wordFour.toUpperCase().split('');

    var decodedOne = '';
    var decodedTwo = '';
    var decodedThree = '';
    var decodedFour = '';

    //Word One
    for(var i=0; i < $arrayWordOne.length; i++){
        var calc = $arrayWordOne[i].charCodeAt(0) - 20;

        if(calc >= 65){
            decodedOne = decodedOne + String.fromCharCode(calc);
        }else{
            calc = $arrayWordOne[i].charCodeAt(0) + 6;
            decodedOne = decodedOne + String.fromCharCode(calc);
        }
    }

    //Word Two
    for(var i=0; i < $arrayWordTwo.length; i++){
        var calc = $arrayWordTwo[i].charCodeAt(0) - 6;
        decodedTwo = decodedTwo + String.fromCharCode(calc);
    }

    //Word Three
    for(var i=0; i < $arrayWordThree.length; i++){
        var calc = $arrayWordThree[i].charCodeAt(0) - 25;

        if(calc >= 65){
            decodedThree = decodedThree + String.fromCharCode(calc);
        }else{
            calc = $arrayWordThree[i].charCodeAt(0) + 2;
            decodedThree = decodedThree + String.fromCharCode(calc);
        }
    }

    //Word Four
    for(var i=0; i < $arrayWordFour.length; i++){
        var calc = $arrayWordFour[i].charCodeAt(0) - 15;

        if(calc >= 65){
            decodedFour = decodedFour + String.fromCharCode(calc);
        }else{
            calc = $arrayWordFour[i].charCodeAt(0) + 11;
            decodedFour = decodedFour + String.fromCharCode(calc);
        }
    }

    $("#result").html('<div class="alert alert-primary" role="alert"> Palabra 1 es: <strong>' + decodedOne + '</strong><br>' +
        'Palabra 2 es: <strong>' + decodedTwo + '</strong><br>' + 'Palabra 3 es: <strong>' + decodedThree + '</strong><br>' +
        'Palabra 4 es: <strong>' + decodedFour + '</strong>' + '</div>');
}